from random import*
import pygame, sys
from pygame.locals import *
pygame.init()



##redimension éléments de l'écran##
#coefficiant de la taille de l'écran
coeff = 0.8
#taille de l'écran, avec la résolution de l'écran(1920,1080)
LARGEUR = (1920*coeff)
HAUTEUR = (1080*coeff)
#taille de la carte, on laisse 30% de la taille de l'écran pour mettre les cartes
HAUTEUR_CARTE_EU = (HAUTEUR-(0.4*HAUTEUR))
LARGEUR_CARTE_EU = (HAUTEUR_CARTE_EU*((3402/2194)))
#coefficiant de proportionnalite écran et carte de l'europe
coeffcarte_ecran =((3402/1920),(2194/1080))
#le fond derriere la carte a la même largeur que la carte mais a la hauteur de l'écran
LARGEUR_FOND = LARGEUR_CARTE_EU
HAUTEUR_FOND = HAUTEUR
#taille des cartes de jeu, on prend 8%de la hauteur pour la taille (deux rangées de cartes en tout dans l'espace de 20%)
HAUTEUR_CARTE = 0.065*HAUTEUR
LARGEUR_CARTE = (HAUTEUR_CARTE*(248/160))


##position des éléments sur l'écran
#coin de la carte de l'Europe.
# y : on laisse 10% d'espace en haut, 20% en bas
# x : on centre la carte
coingauche_carte = ((LARGEUR/2)-(LARGEUR_CARTE_EU/2)), (HAUTEUR - HAUTEUR_CARTE_EU - 0.20*HAUTEUR)
#coin du fond
# y : on le met tout en haut
# x : on centre la carte
coingauche_fond = ((LARGEUR/2)-(LARGEUR_CARTE_EU/2),0)
#position de la premiere carte main
# x : on la positionne au même x que la carte et le fond, mais on la décale un peu
# y : on la positionne à 20% de la hauteur depuis le bas, puis on on redescent un peu vers le bas 
position_carte_main = ((LARGEUR/2)-(LARGEUR_CARTE_EU/2)+0.01*LARGEUR_FOND, HAUTEUR  - 0.20*HAUTEUR + (0.005*LARGEUR))
#position carte dos
# x : la même chose que pour la carte_main
# y : un peu en dessous du haut de l'écran
position_carte_dos = ((LARGEUR/2)-(LARGEUR_CARTE_EU/2)+0.01*LARGEUR_FOND, 0.01*HAUTEUR)
#position cartes visible
# x : la même chose que pour la carte_dos mais écarté (de l'espace de deux cartes) de la carte de dos
# y : un peu en dessous du haut de l'écran
position_carte_visible = ((LARGEUR/2)-(LARGEUR_CARTE_EU/2) + LARGEUR_CARTE*2 , 0.01*HAUTEUR)
#distance entre les cartes
distance_cartes = 0.005*LARGEUR

##ajout des images de couleur, cartes et fond
#ajout de la carte
#et de sa version redimensionnée
image_carte_bleu_original = pygame.image.load("images/cards/eu_WagonCard_blue.png")
image_carte_bleu = pygame.transform.scale(image_carte_bleu_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_noir_original = pygame.image.load("images/cards/eu_WagonCard_black.png")
image_carte_noir = pygame.transform.scale(image_carte_noir_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_orange_original = pygame.image.load("images/cards/eu_WagonCard_brown.png")
image_carte_orange = pygame.transform.scale(image_carte_orange_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_vert_original = pygame.image.load("images/cards/eu_WagonCard_green.png")
image_carte_vert = pygame.transform.scale(image_carte_vert_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_multicolore_original = pygame.image.load("images/cards/eu_WagonCard_loco.png")
image_carte_multicolore = pygame.transform.scale(image_carte_multicolore_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_violet_original = pygame.image.load("images/cards/eu_WagonCard_purple.png")
image_carte_violet = pygame.transform.scale(image_carte_violet_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_rouge_original = pygame.image.load("images/cards/eu_WagonCard_red.png")
image_carte_rouge = pygame.transform.scale(image_carte_rouge_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_blanc_original = pygame.image.load("images/cards/eu_WagonCard_white.png")
image_carte_blanc = pygame.transform.scale(image_carte_blanc_original,(LARGEUR_CARTE,HAUTEUR_CARTE))

image_carte_jaune_original = pygame.image.load("images/cards/eu_WagonCard_yellow.png")
image_carte_jaune = pygame.transform.scale(image_carte_jaune_original,(LARGEUR_CARTE,HAUTEUR_CARTE))
#ajout de la carte de dos
#et sa version redimensionnée
image_carte_de_dos_original = pygame.image.load("images/cards/eu_WagonCard_back.png")
image_carte_de_dos = pygame.transform.scale(image_carte_de_dos_original,(LARGEUR_CARTE,HAUTEUR_CARTE))
#ajout de la carte de l'europe
#et de sa version redimensionnée
image_carte_original = pygame.image.load("images/euMap.jpg")
image_carte = pygame.transform.scale(image_carte_original, (LARGEUR_CARTE_EU, HAUTEUR_CARTE_EU))
#ajout du fond
#et de sa version redimensionnée
image_fond_original = pygame.image.load("images/fond_globe.jpg")
image_fond = pygame.transform.scale(image_fond_original, (LARGEUR_FOND, HAUTEUR_FOND))


##définir les zones de clique
#définir la zone de pioche
zone_carte_de_dos = pygame.Rect(position_carte_dos, (HAUTEUR_CARTE, LARGEUR_CARTE))
#zone des cartes visible (prend la premiere position, et l'espacement entre chaque position de cartes)
espacement = LARGEUR_CARTE + distance_cartes
zone_carte_1 = pygame.Rect(((int(position_carte_visible[0] + espacement * 0)), position_carte_visible[1]),(HAUTEUR_CARTE, LARGEUR_CARTE))
zone_carte_2 = pygame.Rect(((int(position_carte_visible[0] + espacement * 1)), position_carte_visible[1]),(HAUTEUR_CARTE, LARGEUR_CARTE))
zone_carte_3 = pygame.Rect(((int(position_carte_visible[0] + espacement * 2)), position_carte_visible[1]),(HAUTEUR_CARTE, LARGEUR_CARTE))
zone_carte_4 = pygame.Rect(((int(position_carte_visible[0] + espacement * 3)), position_carte_visible[1]),(HAUTEUR_CARTE, LARGEUR_CARTE))
zone_carte_5 = pygame.Rect(((int(position_carte_visible[0] + espacement * 4)), position_carte_visible[1]),(HAUTEUR_CARTE, LARGEUR_CARTE))
#definir les zones de clique des cartes de la main du joueur
espacementx = LARGEUR_CARTE + distance_cartes
espacementy = HAUTEUR_CARTE + distance_cartes
zones_cartes_main = [pygame.Rect((position_carte_main[0] + espacementx*0 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*1 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*2 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*3 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*4 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*5 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*6 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*7 ,position_carte_main[1]),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     
                     pygame.Rect((position_carte_main[0] + espacementx*0 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*1 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*2 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*3 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*4 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*5 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*6 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     pygame.Rect((position_carte_main[0] + espacementx*7 ,position_carte_main[1] + espacementy),(HAUTEUR_CARTE, LARGEUR_CARTE)),
                     ]

#définir la zone de clique pour passer au prochain joueur
# y : au même niveau que la fin de la carte (un peu décalé vers le bas)
# x : au meme niveau que la fin de la carte (un peu décalé vers la droite)
zone_bouton_suivant = pygame.Rect((LARGEUR/2) + LARGEUR_CARTE_EU/2 + 0.05*LARGEUR , (HAUTEUR - 0.20*HAUTEUR) + 0.05*LARGEUR, 0.1*LARGEUR, 0.05*HAUTEUR)
#zone ou l'action est écrit
position_action = pygame.Rect((LARGEUR/2)-(LARGEUR_CARTE_EU/2)+0.01*LARGEUR, 0.15*HAUTEUR,50,50)

#ou l'on met le premier resultat
zone_resultat = pygame.Rect(100,100,5,5)






#couleur des ronds selon la couleur du joueur
bleu=(98, 174, 255)
violet=(156, 98, 255)
rouge=(255, 98, 98)
jaune=(255, 198, 65)
vert=(98, 255, 90)

#police d'écriture
police1 = pygame.font.SysFont("monospace", 17, bold = True)
police2 = pygame.font.SysFont("broadway",25)
police3 = pygame.font.SysFont("broadway",int(coeff*71))


#creation de l'écran
fond_ecran = pygame.image.load("images/fond_globe.jpg")
fond_ecran = pygame.transform.scale(fond_ecran, (LARGEUR,HAUTEUR))
ecran = pygame.display.set_mode((LARGEUR, HAUTEUR))
pygame.display.set_caption('Les aventuriers du rail')

##Fonctions d'affichage
def couleur_vers_carte(couleur,x,y):
    '''reçoit la couleur d'une carte et affiche la carte qui lui correspond
    en position x,y'''
    if couleur == 'jaune':
        ecran.blit(image_carte_jaune,(x,y))
    elif couleur == 'bleu':
        ecran.blit(image_carte_bleu,(x,y))
    elif couleur == 'orange':
        ecran.blit(image_carte_orange,(x,y))
    elif couleur == 'vert':
        ecran.blit(image_carte_vert,(x,y))
    elif couleur == 'blanc':
        ecran.blit(image_carte_blanc,(x,y))
    elif couleur == 'noir':
        ecran.blit(image_carte_noir,(x,y))
    elif couleur == 'multicolore':
        ecran.blit(image_carte_multicolore,(x,y))
    elif couleur == 'rouge':
        ecran.blit(image_carte_rouge,(x,y))
    elif couleur == 'violet':
        ecran.blit(image_carte_violet,(x,y))
            
def affichage_cartes(L,x,y,distance,largeurcarte, hauteurcarte):
    '''Affiche les cartes d'une liste L selon leur couleur les unes après les autres 
    En position x, y et espacées d'une certaine distance
    lorque la longueur limite est atteinte (8 cartes),
    retourne avec le x de départ, et refait la meme chose'''
    x_debut = x
    nombrecarte = 0
    for i in range(len(L)):
        if nombrecarte < 8 :
            couleur_vers_carte(L[i],x,y)
            x = x + largeurcarte + distance
            nombrecarte = nombrecarte +1
        else :
            x = x_debut
            y = y + hauteurcarte + distance
            nombrecarte = 0
            couleur_vers_carte(L[i],x,y)
            x = x + largeurcarte + distance
            nombrecarte = nombrecarte +1          
    pygame.display.update()
    
def affichage_routes(L_routes,L_joueurs):
    '''reçoit la liste des routes L, parcours les centres
    pour chaque route prise par chaque joueur,
    un cercle de differente couleur est affiché a chaque centre de la route'''
    for joueur in L_joueurs:
        for indiceRoute in joueur['routes']:
            route = L_routes[int(indiceRoute)]
            for position in route['centres']:
                pygame.draw.circle(ecran,joueur["couleur"],position,10)
 
def affichage_texte(texte, zone, police, couleur):
    '''on écrit un texte donné dans une zone donnée avec une police donnée
    la zone étant un tuple avec :
    coordonnees (x,y) du coin gauche,
    suivi de la longueur de la zone
    puis de la hauteur de la zone
    couleur avec un tuple'''
    input_rect = pygame.Rect(zone)
    texte_affiche = police.render(texte,True,couleur)
    ecran.blit(texte_affiche,(input_rect))



##fonctions sur les listes de carte

def melanger(L):
    for i in range(len(L)):
        j = randint(0,len(L)-2)
        temp = L[j]
        L[j] = L[j+1]
        L[j+1] = temp
    return L

def tri(L):
    for i in range(len(L)):
        for j in range (len(L)-1):
            if L[j+1] < L[j] :
                temp = L[j]
                L[j] = L[j+1]
                L[j+1] = temp
    return L
    
def listeCarteWagons(L_couleurs):
    '''renvoie la liste cartes, mélangée,
    avec des cartes de chaque couleurs de la liste L_couleurs'''
    cartes=[]
    for i in range(12):
        serie = melanger(L_couleurs)
        cartes = cartes + serie
    return cartes

def listeCartevisible(L):
    '''Crée un liste cartes de 5 cartes en prenant celles de la liste L
    Puis on retire les cartes de la liste L
    '''
    cartes = []
    for i in range(5):
        cartes.append(L[0])
        cartesWagons.pop(0)
    return cartes

def distribuer(L_cartes, L_joueurs):
    '''Pour chaque joueur de la liste L_joueurs,
    On lui rajoute trois cartes qui viennent de la liste L_cartes
    Puis on les retire de la liste L_cartes
    '''
    for indice_joueur in range(len(L_joueurs)):
        for i in range(3):
            L_joueurs[indice_joueur]["main"].append(L_cartes[0])
            L_cartes.pop(0)
            
            
      
##fonctions d'action de tour

def piocher(joueur, L_cartes):
    '''on met le premiere carte de la liste L_cartes dans la main
    du joueur puis on retire celle-ci de la liste L_cartes'''
    joueur['main'].append(L_cartes[0])
    L_cartes.pop(0)
    affichage_plateau()

def choisirCarte(joueur, L_cartes_visibles, L_cartes, indicecarte):
    '''
    ajoute la carte choisie dans la main du joueur
    remplace la carte choisie dans la pioche par une carte wagon
    '''
    joueur['main'].append(L_cartes_visibles[indicecarte])
    L_cartes_visibles[indicecarte] = L_cartes.pop(0)
    affichage_plateau()

def trouverRoute(position, L_routes):
    '''
    donne l'indice de la route (parmis la liste L_routes)
    la plus proche de là où on a cliquer (position)
    '''
    posproche = (0,0)
    indiceroute = -1
    for route in range (len(L_routes)):
        for i in range(len(L_routes[route]['centres'])):
            if distance(position,posproche)>distance(position,L_routes[route]['centres'][i]):
                posproche=L_routes[route]['centres'][i]
                indiceroute = route
    return indiceroute

def distance(position1, position2):
    '''calcul le carré de la distance entre deux position(x,y)
    '''
    return (position2[0]-position1[0])**2+(position2[1]-position1[1])**2

def choixRoute(route, joueur):
    '''renvoie un booléen si oui ou non la route peut être faite par la joueur actuel
    selon plusieurs critères :
    Si le joueur a assez de wagons
    Si la route est n'est pas prise par un autre joueur
    si la route est grise : si l'on possède autant de cartes que la longueur de la route
    si la route est d'une couleur : si l'on possède autant de cartes
    (de la couleur de la route ou multicolore) que la longueur de la route
    la variable global action permet d'écrire sur l'interface graphique 
    '''
    
    global action
    
    if joueur['nombreWagons'] < route['longueur'] :
        print('pas assez de wagons')
        action = 'pas assez de wagons '
        affichage_plateau()
        return False
    elif route['prise'] == True :
        print('La route est déja prise')
        action = 'La route est déja prise'
        affichage_plateau()
        return False
    elif route['couleur'] == 'gris' :
        if route['longueur'] > len(joueur['main']) :
            print('pas assez de cartes Wagons')
            action = 'pas assez de  cartes wagons '
            affichage_plateau()
            return False
    else :#si c'est une couleur
        compte = route['longueur']
        for i in range(len(joueur['main'])):
                    if joueur["main"][i] == route["couleur"] or joueur["main"][i]=="multicolore":
                        compte = compte-1
        if compte > 0 : #si on a compté moins de carte de couleur ou multicolore que nécessaire
            print('pas assez de cartes Wagons')
            print(route['id'])
            action = 'pas assez de  cartes wagons '
            affichage_plateau()
            return False
    print("la route peut être prise")
    action = 'Choisissez vos cartes '
    affichage_plateau()
    return True
            
def creer_route(L_zones,L_routes, indiceroute, joueur):
    '''on choisi les cartes que l'on veut pour creer la route
L_zones : liste des differentes zones pour cliquer
on cherche la position dans L_zone
l'indice correspondant est l'indice de la carte dans la L_cartes'''
    L_main = joueur['main']
    longueur_route = L_routes[indiceroute]['longueur']
    compte = longueur_route
    while compte != 0:
        decalageindice = 0
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONDOWN:
                for i in range (len(L_zones)) :
                    if L_zones[i].collidepoint(event.pos) and L_routes[indiceroute]['couleur'] == 'gris' :
                        L_main.remove(L_main[i])
                        compte = compte-1
                        decalageindice = decalageindice + 1
                        affichage_plateau()
                    elif L_zones[i].collidepoint(event.pos) and (L_main[i] == L_routes[indiceroute]['couleur'] or L_main[i] == 'multicolore') :
                        L_main.remove(L_main[i])
                        compte = compte-1
                        decalageindice = decalageindice + 1
                        affichage_plateau()
    joueur["routes"].append(str(indiceroute))
    joueur["nombreWagons"]=joueur["nombreWagons"]-longueur_route
    L_routes[indiceroute]['prise']= True
    print("route prise")
    
    
    
    
###Fonctions de tour de jeu

def choix_joueurs():
    couleur = [bleu, violet, rouge, jaune, vert]
    continuer = True
    nombre_joueurs = 0
    listeJoueurs = []
    nom = ["", "", "", "",""]
    affichage_choix_joueurs(nombre_joueurs, listeJoueurs, 100, 100, 70)
    while continuer == True:
        for event in pygame.event.get():
                            #Si on clique sur la croix
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
    
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_2:
                    nombre_joueurs = 2
                    affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
                elif event.key == pygame.K_3:
                    nombre_joueurs = 3
                    affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
                elif event.key == pygame.K_4:
                    nombre_joueurs = 4
                    affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
                elif event.key == pygame.K_5:
                    nombre_joueurs = 5
                    affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
                elif event.key == pygame.K_RETURN and nombre_joueurs > 0:
                    continuer = False
    print("choix nom")
    for i in range (nombre_joueurs) :
        continuer = True
        affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
        while continuer == True:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN and len(nom[i]) > 3:
                        listeJoueurs.append({'nom': nom[i] ,'main':[],'couleur': couleur[i],'nombreWagons':45, "routes":[]})
                        affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
                        continuer = False
                    else:
                        key_name = pygame.key.name(event.key)
                        nom[i] = nom[i]+ key_name
                        print(key_name)
                        affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
    affichage_choix_joueurs(nombre_joueurs, nom, 100, 100, 70)
    print(listeJoueurs)
    return listeJoueurs

def affichage_choix_joueurs(nombrejoueurs, nom, x, y, distance) :
    texte = "nombres de joueurs : " + str(nombrejoueurs)
    ecran.fill((0, 0, 0))
    pygame.display.flip()
    zone = pygame.Rect(x,y,100,100)
    affichage_texte(texte, zone, police2, (255,255,0) )
    pygame.display.flip()
    for i in range(nombrejoueurs) :
        y = y + distance
        zone = pygame.Rect(x,y,100,100)
        texte = "joueur " + str(i+1) +" : " + nom[i]
        affichage_texte(texte, zone, police2, (255,255,0) )
        pygame.display.flip()
        
        
def demandeActionJeu():
    '''Selon l'endroite ou on clique, on peut faire différentes actions
    (répétés deux fois si l'on choisi de piocher ou de prendre une carte visible)
    lorsqu'on clique, si on clique:
    Sur la zone de la carte retournée, on pioche (une action possible en moins)
    Sur un des zones des cartes visible, on prend la carte visible correspondante (une action possible en moins)
    sinon, on considere que le joueur a cliqué sur une route
    la variable global action permet d'écrire sur l'interface graphique 
    '''
    
    global action
    
    choix = False
    compte = 0
    while not choix:
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONDOWN:
                action=""
                if zone_carte_de_dos.collidepoint(event.pos) and len(joueurCourant['main'])<16 and compte != 2:
                    piocher(joueurCourant, cartesWagons)
                    compte = compte + 1
                elif zone_carte_1.collidepoint(event.pos) and len(joueurCourant['main'])<16 and compte != 2:
                    choisirCarte(joueurCourant, cartesVisibles, cartesWagons, 0)
                    compte = compte + 1
                elif zone_carte_2.collidepoint(event.pos) and len(joueurCourant['main'])<16 and compte != 2:
                    choisirCarte(joueurCourant, cartesVisibles, cartesWagons, 1)
                    compte = compte + 1
                elif zone_carte_3.collidepoint(event.pos) and len(joueurCourant['main'])<16 and compte != 2:
                    choisirCarte(joueurCourant, cartesVisibles, cartesWagons, 2)
                    compte = compte + 1
                elif zone_carte_4.collidepoint(event.pos) and len(joueurCourant['main'])<16 and compte != 2:
                    choisirCarte(joueurCourant, cartesVisibles, cartesWagons, 3)
                    compte = compte + 1
                elif zone_carte_5.collidepoint(event.pos) and len(joueurCourant['main'])<16 and compte != 2:
                    choisirCarte(joueurCourant, cartesVisibles, cartesWagons, 4)
                    compte = compte + 1
                elif zone_bouton_suivant.collidepoint(event.pos) :
                    choix= True
                elif compte == 0 :
                    indiceroute = trouverRoute(pygame.mouse.get_pos(), listeRoutes)
                    if choixRoute(listeRoutes[indiceroute], joueurCourant)==True :
                        creer_route(zones_cartes_main, listeRoutes, indiceroute, joueurCourant)
                        action= "fin du tour, appuyez sur suivant"
                        affichage_plateau()
                if compte == 2 :
                    action= "fin du tour, appuyez sur suivant"
                    affichage_plateau()
    action = ""
    affichage_plateau()
    

def affichage_plateau():
    '''affiche le fond, qui ne rempli qu'une partie de l'écran,
    les cartes(pioche, pioche visible, main du joueur)
    les routes prise
    les différents textes'''
    #affichage fond de l'écran
    ecran.blit(fond_ecran,(0,0))
    #affichage fond
    image_fond.fill((152,69,55))
    ecran.blit(image_fond,coingauche_fond)
    #affichage carte de l'europe
    ecran.blit(image_carte, coingauche_carte)
    #affichage cartes
    ecran.blit(image_carte_de_dos, position_carte_dos)
    affichage_cartes(joueurCourant['main'],position_carte_main[0],position_carte_main[1],distance_cartes,LARGEUR_CARTE, HAUTEUR_CARTE)
    affichage_cartes(cartesVisibles,position_carte_visible[0],position_carte_visible[1],distance_cartes,LARGEUR_CARTE, HAUTEUR_CARTE)
    #affichage routes
    affichage_routes(listeRoutes,listeJoueurs)
    #affichage nom du joueur qui joue
    affichage_texte(affichage_nom_joueur, ((LARGEUR/2) + LARGEUR_CARTE_EU/2 + 0.05*LARGEUR , 0.05*HAUTEUR , 150, 30) , police1, (255,255,255))
    affichage_texte("nombre de wagons restants : " + str(joueurCourant['nombreWagons']) , ((LARGEUR/2) + LARGEUR_CARTE_EU/2 + 0.01*LARGEUR , 0.09*HAUTEUR , 150, 30) , police1, (255,255,255))
    #affichage du bouton pour passer au tour suivant
    affichage_texte("suivant", zone_bouton_suivant , police3, (255,255,255))
    #affichage d'une phrase lié à l'action
    affichage_texte(action,position_action , police2, (255,255,255))
    pygame.display.flip()
    
def poursuite_jeu(L_route, L_joueurs):
    nombre_routes_prises = 0
    for r in (L_route) :
        if r['prise'] == True :
            nombre_routes_prises = nombre_routes_prises + 1
        if nombre_routes_prises == len(L_route) :
            print('toutes les routes ont été prises, fin de la partie')
            return False
    for j in (L_joueurs):
        if j['nombreWagons'] < 3 :
            print('un joueur possède moins de 3 wagons, fin de la partie')
            return False
    return True
    
def affichage_resultat(x,y, L_joueurs, zone, distance):
    '''met un écran noir
    affiche les resultats
    met a jour l'affichage'''
    ecran.fill((0, 0, 0))
    affichage_texte("fin de la partie, résultats : ", zone, police2, (255,255,0))
    for i in range(len(L_joueurs)) :
        y = y + distance
        zone = pygame.Rect(x,y,100,100)
        texte = L_joueurs[i]['nom'] + " : " + str(L_joueurs[i]['points']) + " points"
        affichage_texte(texte, zone, police2, (255,255,0) )
        pygame.display.flip()
  
    
#### Debut du programme principal. ########################

#Initialisation des variables
    
#dictionnaires des routes
listeRoutes =[{'couleur': 'orange', 'id': 'EDI-LON ORA', 'longueur': 4, 'centres': [(624, 278), (636, 310), (650, 342), (662, 376)], 'prise': False},
               {'couleur': 'orange', 'id': 'BRE-DIE ORA', 'longueur': 2, 'centres': [(588, 500), (624, 490)], 'prise': False},
               {'couleur': 'orange', 'id': 'PAR-FRA ORA', 'longueur': 3, 'centres': [(748, 544), (784, 530), (816, 506)], 'prise': False},
               {'couleur': 'orange', 'id': 'MAD-CAD ORA', 'longueur': 3, 'centres': [(566, 794), (586, 824), (568, 842)], 'prise': False},
               {'couleur': 'orange', 'id': 'MUN-WIE ORA', 'longueur': 3, 'centres': [(912, 552), (946, 568), (982, 556)], 'prise': False},
               {'couleur': 'orange', 'id': 'ZAG-BUD ORA', 'longueur': 2, 'centres': [(1018, 626), (1038, 592)], 'prise': False},
               {'couleur': 'orange', 'id': 'SMO-MOS ORA', 'longueur': 2, 'centres': [(1360, 402), (1398, 390)], 'prise': False},
               {'couleur': 'orange', 'id': 'SMY-ANG ORA', 'longueur': 3, 'centres': [(1240, 850), (1278, 850), (1316, 838)], 'prise': False},
               
               {'couleur': 'bleu', 'id': 'WIL-PET BLE', 'longueur': 4, 'centres': [(1240, 372), (1264, 342), (1284, 310), (1310, 280)], 'prise': False},
               {'couleur': 'bleu', 'id': 'LIS-CAD BLE', 'longueur': 2, 'centres': [(490, 824), (514, 844)], 'prise': False},
               {'couleur': 'bleu', 'id': 'MUN-VEN BLE', 'longueur': 3, 'centres': [(896, 566), (904, 600), (904, 600)], 'prise': False},
               {'couleur': 'bleu', 'id': 'BRU-FRA BLE', 'longueur': 2, 'centres': [(774, 452), (804, 464)], 'prise': False},
               {'couleur': 'bleu', 'id': 'PAM-PAR BLE', 'longueur': 4, 'centres': [(644, 666), (666, 632), (680, 600), (688, 562)], 'prise': False},
               {'couleur': 'bleu', 'id': 'ESS-BER BLE', 'longueur': 2, 'centres': [(890, 410), (924, 416)], 'prise': False},
               {'couleur': 'bleu', 'id': 'WIE-WAR BLE', 'longueur': 4, 'centres': [(1030, 530), (1054, 508), (1084, 476), (1102, 450)], 'prise': False},
               {'couleur': 'bleu', 'id': 'SOF-CON BLE', 'longueur': 3, 'centres': [(1172, 722), (1204, 738), (1232, 752)], 'prise': False},
              
              {'couleur': 'violet', 'id': 'LIS-MAD VIO', 'longueur': 3, 'centres': [(474, 769), (486, 746), (526, 757)], 'prise': False},
              {'couleur': 'violet', 'id': 'BRE-PAM VIO', 'longueur': 4, 'centres': [(640, 638), (635, 599), (625, 566), (594, 540)], 'prise': False},
              {'couleur': 'violet', 'id': 'DIE-PAR', 'longueur': 1, 'centres': [(682, 510)], 'prise': False},
              {'couleur': 'violet', 'id': 'MAR-ZUR VIO', 'longueur': 3, 'centres': [(811, 655), (811, 655), (820, 617)], 'prise': False},
              {'couleur': 'violet', 'id': 'FRA-MUN VIO', 'longueur': 2, 'centres': [(844, 510), (861, 532)], 'prise': False},
              {'couleur': 'violet', 'id': 'BER-WAR VIO', 'longueur': 4, 'centres': [(974, 416), (1013, 407), (1049, 403), (1086, 405)], 'prise': False},
              {'couleur': 'violet', 'id': 'BUD-SAR VIO', 'longueur': 4, 'centres': [(1063, 596), (1071, 633), (1075, 671), (1075, 671)], 'prise': False},
              {'couleur': 'violet', 'id': 'ATH-SOF VIO', 'longueur': 3, 'centres': [(1119, 797), (1112, 758), (1129, 722)], 'prise': False},
              
              {'couleur': 'vert', 'id': 'PAM-PAR VER', 'longueur': 4, 'centres': [(662, 670), (680, 638), (692, 603), (702, 564)], 'prise': False},
              {'couleur': 'vert', 'id': 'DIE-BRU VER', 'longueur': 2, 'centres': [(688, 480), (717, 459)], 'prise': False},
              {'couleur': 'vert', 'id': 'FRA-ESS VER', 'longueur': 2, 'centres': [(850, 459), (870, 439)], 'prise': False},
              {'couleur': 'vert', 'id': 'BER-WIE VER', 'longueur': 3, 'centres': [(958, 457), (972, 491), (991, 521)], 'prise': False},
              {'couleur': 'vert', 'id': 'ZUR-VEN VER', 'longueur': 2, 'centres': [(850, 605), (884, 622)], 'prise': False},
              {'couleur': 'vert', 'id': 'SAR-ATH VER', 'longueur': 4, 'centres': [(1079, 723), (1074, 794), (1097, 810)], 'prise': False},
              {'couleur': 'vert', 'id': 'RIG-WIL VER', 'longueur': 4, 'centres': [(1147, 285), (1151, 321), (1175, 349), (1206, 369)], 'prise': False},
              {'couleur': 'vert', 'id': 'KHA-ROS VER', 'longueur': 2, 'centres': [(1432, 527), (1442, 551)], 'prise': False},
              
              {'couleur': 'noir', 'id': 'MAD-PAM NOI', 'longueur': 3, 'centres': [(558, 750), (582, 724), (612, 698)], 'prise': False},
              {'couleur': 'noir', 'id': 'BRE-PAR NOI', 'longueur': 3, 'centres': [(596, 523), (633, 527), (670, 532)], 'prise': False},
              {'couleur': 'noir', 'id': 'BRU-AMS NOI', 'longueur': 1, 'centres': [(756, 429)], 'prise': False},
              {'couleur': 'noir', 'id': 'EDI-LON NOI', 'longueur': 4, 'centres': [(607, 278), (622, 310), (637, 347), (651, 379)], 'prise': False},
              {'couleur': 'noir', 'id': 'FRA-BER NOI', 'longueur': 3, 'centres': [(857, 471), (893, 451), (925, 438)], 'prise': False},
              {'couleur': 'noir', 'id': 'VEN-ROM NOI', 'longueur': 2, 'centres': [(914, 653), (922, 690)], 'prise': False},
              {'couleur': 'noir', 'id': 'ANG-ERZ NOI', 'longueur': 3, 'centres': [(1363, 834), (1400, 839), (1423, 824)], 'prise': False},
              {'couleur': 'noir', 'id': 'DAN-RIG NOI', 'longueur': 3, 'centres': [(1074, 323), (1090, 290), (1124, 266)], 'prise': False},
              
              {'couleur': 'blanc', 'id': 'MAD-PAM BLA', 'longueur': 3, 'centres': [(566, 757), (594, 732), (622, 707)], 'prise': False},
              {'couleur': 'blanc', 'id': 'PA-FRA BLA', 'longueur': 3, 'centres': [(742, 529), (777, 513), (806, 496)], 'prise': False},
              {'couleur': 'blanc', 'id': 'AM-FRA BLA', 'longueur': 2, 'centres': [(789, 427), (816, 452)], 'prise': False},
              {'couleur': 'blanc', 'id': 'ROM-BRI BLA', 'longueur': 3, 'centres': [(949, 706), (985, 721), (985, 721)], 'prise': False},
              {'couleur': 'blanc', 'id': 'WIE-BUD BLA', 'longueur': 1, 'centres': [(1024, 569)], 'prise': False},
              {'couleur': 'blanc', 'id': 'BUC-SEV BLA', 'longueur': 4, 'centres': [(1230, 623), (1263, 607), (1302, 607), (1331, 633)], 'prise': False},
              {'couleur': 'blanc', 'id': 'PET-MOS BLA', 'longueur': 4, 'centres': [(1347, 261), (1380, 280), (1403, 311), (1411, 346)], 'prise': False},
              {'couleur': 'blanc', 'id': 'KOB-STO BLA', 'longueur': 3, 'centres': [(944, 292), (972, 268), (1004, 246)], 'prise': False},
              
              {'couleur': 'jaune', 'id': 'MA-BAR JAU', 'longueur': 2, 'centres': [(588, 780), (623, 782)], 'prise': False},
              {'couleur': 'jaune', 'id': 'PAR-BRU JAU', 'longueur': 2, 'centres': [(714, 507), (730, 476)], 'prise': False},
              {'couleur': 'jaune', 'id': 'AMS-ESS JAU', 'longueur': 3, 'centres': [(767, 378), (796, 373), (829, 394)], 'prise': False},
              {'couleur': 'jaune', 'id': 'KOB-STO JAU', 'longueur': 3, 'centres': [(935, 284), (960, 256), (998, 237)], 'prise': False},
              {'couleur': 'jaune', 'id': 'ZU-MUN JAU', 'longueur': 2, 'centres': [(847, 572), (875, 548)], 'prise': False},
              {'couleur': 'jaune', 'id': 'BER-WAR JAU', 'longueur': 4, 'centres': [(979, 432), (1012, 423), (1052, 421), (1085, 421)], 'prise': False},
              {'couleur': 'jaune', 'id': 'WIL-SMO JAU', 'longueur': 3, 'centres': [(1253, 377), (1277, 363), (1305, 384)], 'prise': False},
              {'couleur': 'jaune', 'id': 'BUC-CON JAU', 'longueur': 3, 'centres': [(1222, 667), (1236, 703), (1249, 736)], 'prise': False},
              
              {'couleur': 'rouge', 'id': 'PAM-MAR ROU', 'longueur': 4, 'centres': [(674, 699), (696, 681), (723, 660), (762, 670)], 'prise': False},
              {'couleur': 'rouge', 'id': 'PAR-BRU ROU', 'longueur': 2, 'centres': [(726, 514), (744, 481)], 'prise': False},
              {'couleur': 'rouge', 'id': 'FRA-BER ROU', 'longueur': 3, 'centres': [(864, 482), (897, 468), (928, 452)], 'prise': False},
              {'couleur': 'rouge', 'id': 'ZAG-SAR ROU', 'longueur': 4, 'centres': [(996, 666), (1017, 695), (1053, 702), (1053, 702)], 'prise': False},
              {'couleur': 'rouge', 'id': 'ERZ-SOC ROU', 'longueur': 3, 'centres': [(1425, 769), (1430, 734), (1434, 699)], 'prise': False},
              {'couleur': 'rouge', 'id': 'WAR-WIL ROU', 'longueur': 3, 'centres': [(1132, 398), (1160, 375), (1200, 385)], 'prise': False},
              {'couleur': 'rouge', 'id': 'KYI-SMO ROU', 'longueur': 3, 'centres': [(1294, 474), (1328, 460), (1340, 424)], 'prise': False},
              {'couleur': 'rouge', 'id': 'WIE-BUD ROU', 'longueur': 1, 'centres': [(1032, 554)], 'prise': False}]
#mise à l'échelle
distancex = ((LARGEUR/2)-(LARGEUR_CARTE_EU/2))
distancey = (HAUTEUR - HAUTEUR_CARTE_EU - 0.20*HAUTEUR)
for i in range(len(listeRoutes)):
        for j in range(len(listeRoutes[i]['centres'])):
            listeRoutes[i]['centres'][j] = ((listeRoutes[i]['centres'][j][0]*coeff), (listeRoutes[i]['centres'][j][1]*coeff))
        

#liste des cartes
couleurs=['jaune','bleu','vert','noir','blanc','orange','violet','rouge','multicolore']
cartesWagons = listeCarteWagons(couleurs)
cartesVisibles = listeCartevisible(cartesWagons)

#liste des joueurs
listeJoueurs = choix_joueurs()


#distribution et tri des trois premières cartes
distribuer(cartesWagons, listeJoueurs)
for i in (listeJoueurs):
    i['main']=tri(i['main'])
#Début du tour de jeu
action =""
tour = True
while tour == True :
    for i in range (len(listeJoueurs)):
        if tour == True :
                joueurCourant = listeJoueurs[i]
                
                #affichage sur la console
                print("Au tour du", joueurCourant['nom'])
                print("Main du ", joueurCourant['nom'], " : ", joueurCourant['main'])
                print("cartes visibles : ", cartesVisibles)
                #texte à afficher sur l'écran
                affichage_nom_joueur = joueurCourant['nom']
                
                #affichage sur l'écran graphique
                tri(joueurCourant['main'])
                affichage_plateau()
                demandeActionJeu()
                affichage_plateau()
                
                #si la pioche se fini
                if len(cartesWagons)<= 1 :
                    cartesWagons = listeCarteWagons()
                    
                #affichage console de la nouvelle main   
                print("Main du ", joueurCourant['nom'], " : ", joueurCourant['main'])
                
                #on verifie si le tour peut se poursuivre
                tour = (poursuite_jeu(listeRoutes, listeJoueurs))
                

##fin du tour, comptage des points
#regarde chaque joueurs et voit les routes prises selon la longueur de la route, on gagne un certain nombre de point
for joueur in (listeJoueurs):
    joueur['points'] = 0
    for indiceroute in (joueur['routes']) :
        longueur = listeRoutes[int(indiceroute)]['longueur']
        if longueur == 1 :
            joueur['points'] = joueur['points']+1
        if longueur == 2 :
            joueur['points'] = joueur['points']+2
        if longueur == 3 :
            joueur['points'] = joueur['points']+4
        if longueur == 4 :
            joueur['points'] = joueur['points']+7
        if longueur == 5 :
            joueur['points'] = joueur['points']+10
        if longueur == 6 :
            joueur['points'] = joueur['points']+15
#affichage console
for i in (listeJoueurs):
    print(i['nom'] + " a gagné " + str(i['points']) +" point !")

#tri par nombre de point des joueurs
listeJoueurs_tries = sorted(listeJoueurs, key=lambda joueur: joueur['points'], reverse = True)
#affichage écran graphique
affichage_resultat(180,100, listeJoueurs_tries, zone_resultat, 70)
#pour quitter
while True:
#on passe en revue les événements observés
    for event in pygame.event.get():
                #Si on clique sur la croix
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
